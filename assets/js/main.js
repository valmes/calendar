// essa função roda sempre que o documento termina de carregar os elementos
// é sempre interessante que esse tipo de script esteja no final do documento, depois do body
// justamente pq ele utiliza e mexe com os elementos do dom
// o importante é já ter carregado os elementos que serão utilizados
$(function (){
	// a ideia dessa função é ir colocando todo o html do calendário em uma srting
	// e depois inserir o html com jquery
	function draw_calendar(m, y)
	{
		var calendar = '';

		// inicio a tabela
		calendar += '<tbody>';

		/* table headings */
		var headings = ['dom','seg','ter','qua','qui','sex','sáb'];
		// inicio a row dos weekdays
		calendar += '<tr class="calendar-head week"><td class="calendar-day-head">';
		// a função join concatena cada item do array headings com a string dentro dele
		// faz a mesma coisa que eu fiz com o for dos meses
		calendar += headings.join('</td><td class="calendar-day-head">');
		calendar += '</td></tr>';

		/* days and weeks vars now ... */

		// descobre em que dia da semana começa o mês atual (0-6)
		var running_day = new Date(y, m, 1).getDay();
		// descobre quantos dias tem o mês anterior
		var days_in_prev = new Date(y, m, 0).getDate();
		// descobre quantos dias tem o mês atual
		var days_in_month = new Date(y, m + 1, 0).getDate();
		// variável auxiliar para a contagem de dias durante a semana (1-7)
		var days_in_this_week = 1;
		// contador de dias, pra ter controle do fim do mês
		var day_counter = 0;
		// descobre qual é o dia que inicia a primeira semana da tabela
		var prev_month_start = days_in_prev - running_day + 1;

		/* row for week one */
		calendar += '<tr class="calendar-row">';

		/* print "last days" until the first of the current week */
		for (var i = 0; i < running_day; i++) {
			// insiro o dia do mês anterior
			calendar += '<td class="calendar-day-np"><span>' + prev_month_start + '</span></td>';
			// incremento o dia da semana
			days_in_this_week++;
			// incremento o dia do mês anterior
			prev_month_start++;
		};

		/* keep going with days.... */
		// começo com os dias do mês atual
		// com esse for eu faço o mesmo processo para cada dia do mês
		for (var list_day = 1; list_day <= days_in_month; list_day++) {
			// abro o td
			calendar += '<td class="calendar-day">';
			// insiro o número
			calendar += '<span>' + list_day + '<span>';

			/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
			// esse pedaço será útil quando tiver algum evento
			// calendar += '<p> </p><p> </p>';
			// fecho o td
			calendar += '</td>';

			// se running_day for igual a 6 quer dizer que é sábado, precisa fechar a row
			if (running_day == 6) {
				calendar += '</tr>';
				// verifica se o dia atual é o último dia do mês
				// se não for, tem row nova
				if ((day_counter + 1) != days_in_month) {
					calendar += '<tr class="calendar-row">';
				};
				// reinicia o running_day para o começo da semana
				running_day = -1;
				// reinicia o days_in_this_week para o começo da semana
				days_in_this_week = 0;
			};
			// incremento as variáveis
			days_in_this_week++; 
			running_day++; 
			day_counter++;
		};

		/* finish the rest of the days in the week */
		// completo a tabela com dias do mês anterior
		var next_month_day = 1;
		// verifica se a semana acabou ou não
		// só continua preenchendo se a semana não reiniciou no fim do for
		if (days_in_this_week != 1) {
			// faço isso "quantas células faltarem" pra preencher a row
			for (var x = 1; x <= (8 - days_in_this_week); x++) {
				// insiro o dia do próximo mês e incremento
				calendar += '<td class="calendar-day-np"><span>' + next_month_day + '<span></td>';
				next_month_day++;
			};
		};

		/* final row */
		// fecho a row e tbody
		calendar += '</tr></tbody>';

		/* all done, return result */
		// insiro a string resultante na tabela
		$('#table').html(calendar);
		$('#table').data('month', m);
		$('#table').data('year', y);
		// reinicializo a função que fica "esperando" o clique das setas
		// se eu não fizer isso dá erro
		// pq eu inseri elementos depois de ter carregado a função no document.ready()
		// e aí a função não funciona
		changeMonthHolder();
	}

	function onLoad()
	{
		// quando o dom carrega ele pega o ano e o mês atuais e desenha
		var month = new Date().getMonth();
		var year = new Date().getFullYear();
		draw_calendar(month, year);
	}

	function prevMonth()
	{
		var month = $('#table').data('month');
		var year = $('#table').data('year');
		month--;
		if (month == -1) {
			month = 11;
			year--;
		};
		$('.slick-slider').slick('slickPrev');
		draw_calendar(month, year);
	}

	function nextMonth()
	{
		var month = $('#table').data('month');
		var year = $('#table').data('year');
		month++;
		if (month == 12) {
			month = 0;
			year++;
		};
		$('.slick-slider').slick('slickNext');
		draw_calendar(month, year);
	}
	
	function changeMonthHolder() {
		// a cada clique no botão com a classe change-month
		$('.change-month').off().on('click', function(event) {
			// event.preventDefault();
			// se o id do botão clicado for prev
			if ($(this).attr('id') == 'prev') {
				prevMonth();
			} else {
				nextMonth();
			};
		});
	}

	onLoad();
});