<html>
	<head>
		<title>Our Calendar</title>
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="assets/css/slick.css">
		<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
	</head>
	<body>
		<div class="calendar">
			<div class="calendar-head icons">
				icones
			</div>
			<div class="calendar-head month">
				<button id="prev" class="change-month">Anterior</button>
				<button id="next" class="change-month">Próximo</button>
				<div class="slick-slider">
					<div class="each-month">janeiro</div>
					<div class="each-month">fevereiro</div>
					<div class="each-month">março</div>
					<div class="each-month">abril</div>
					<div class="each-month">maio</div>
					<div class="each-month">junho</div>
					<div class="each-month">julho</div>
					<div class="each-month">agosto</div>
					<div class="each-month">setembro</div>
					<div class="each-month">outubro</div>
					<div class="each-month">novembro</div>
					<div class="each-month">dezembro</div>
				</div>
			</div>
			<table id="table">
				<tbody>
					<tr class="calendar-head week">
						<td class="calendar-day-head">dom</td>
						<td class="calendar-day-head">seg</td>
						<td class="calendar-day-head">ter</td>
						<td class="calendar-day-head">qua</td>
						<td class="calendar-day-head">qui</td>
						<td class="calendar-day-head">sex</td>
						<td class="calendar-day-head">sáb</td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day-np"><span>26</span></td>
						<td class="calendar-day-np"><span>27</span></td>
						<td class="calendar-day-np"><span>28</span></td>
						<td class="calendar-day-np"><span>29</span></td>
						<td class="calendar-day-np"><span>30</span></td>
						<td class="calendar-day-np"><span>31</span></td>
						<td class="calendar-day"><span>1<span><p> </p><p> </p></span></span></td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day"><span>2<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>3<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>4<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>5<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>6<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>7<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>8<span><p> </p><p> </p></span></span></td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day"><span>9<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>10<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>11<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>12<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>13<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>14<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>15<span><p> </p><p> </p></span></span></td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day"><span>16<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>17<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>18<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>19<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>20<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>21<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>22<span><p> </p><p> </p></span></span></td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day"><span>23<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>24<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>25<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>26<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>27<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>28<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>29<span><p> </p><p> </p></span></span></td>
					</tr>
					<tr class="calendar-row">
						<td class="calendar-day"><span>30<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day"><span>31<span><p> </p><p> </p></span></span></td>
						<td class="calendar-day-np"><span>1</span></td>
						<td class="calendar-day-np"><span>2</span></td>
						<td class="calendar-day-np"><span>3</span></td>
						<td class="calendar-day-np"><span>4</span></td>
						<td class="calendar-day-np"><span>5</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript" src="assets/js/slick.js"></script>
	<script>
		$('.slick-slider').slick({
			centerMode: true,
  			variableWidth: true,
			slidesToShow: 1,
			prevArrow: $('#prev'),
			nextArrow: $('#next'),
			initialSlide: new Date().getMonth()
		});
	</script>
</html>