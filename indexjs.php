<html>
<head>
	<title>Our Calendar</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="assets/css/styles.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<div class="calendar">
		<div class="calendar-head icons">
			icones
		</div>
		<div class="calendar-head month">
			<button id="prev" class="change-month">Anterior</button>
			<button id="next" class="change-month">Próximo</button>
			<div class="slick-slider">
				<div class="each-month">janeiro</div>
				<div class="each-month">fevereiro</div>
				<div class="each-month">março</div>
				<div class="each-month">abril</div>
				<div class="each-month">maio</div>
				<div class="each-month">junho</div>
				<div class="each-month">julho</div>
				<div class="each-month">agosto</div>
				<div class="each-month">setembro</div>
				<div class="each-month">outubro</div>
				<div class="each-month">novembro</div>
				<div class="each-month">dezembro</div>
			</div>
		</div>
		<table id="table">
		</table>
	</div>
</div>
</body>
<script type="text/javascript" src="assets/js/slick.js"></script>
<script src="assets/js/main.js"></script>
<script>
	$('.slick-slider').slick({
		centerMode: true,
		variableWidth: true,
		slidesToShow: 1,
		prevArrow: $('#prev'),
		nextArrow: $('#next'),
		initialSlide: new Date().getMonth()
	});
</script>
</html>